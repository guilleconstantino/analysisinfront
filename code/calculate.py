import json
import operator
import math
import copy

def calculateAverage(l):
    return sum(l) / float(len(l))

def calculateStandardDeviation(l):
    variance = 0
    avg = calculateAverage(l)
    for i in range(len(l)):
        variance = variance + pow(l[i] - avg, 2)
    variance = variance / len(l)
    return math.sqrt(variance)

def openData(fileName):
    file = open(fileName, 'r')
    return json.load(file)

def extractValues(data, field, type=str):
    valuesList = []
    for i in range(len(data)):
        valuesList.append(type(data[i].get(field)))
    return valuesList

def writeData(fileName, data):
    outputStringData = json.dumps(data, indent=2)
    f = open(fileName, 'w')
    f.write(outputStringData)
    f.close()

# Calculate Bollinger
def calculateBollinger(data):
    valuesList = extractValues(data, 'last', float)
    for i in range(20, len(data) + 1): 
        l = valuesList[i - 20:i]
        middleBand = calculateAverage(l)
        deviation = calculateStandardDeviation(l)
        
        data[i - 1]['middleBand'] = '%.2f' % (middleBand)
        data[i - 1]['deviation'] = '%.2f' % (deviation)
        data[i - 1]['upperBand'] = '%.2f' % (middleBand + 2 * deviation)
        data[i - 1]['lowerBand'] = '%.2f' % (middleBand - 2 * deviation)
    return data

def calculateRSI(data):
    valuesList = extractValues(data, 'last', float)
    gains = []
    losses = []
    for i in range(1, len(data)):
        change = round(valuesList[i] - valuesList[i - 1], 2)
        data[i]['change'] = change
        data[i]['gain'] = gain = change > 0 and change or 0
        gains.append(gain)

        data[i]['loss'] = loss = change < 0 and -change or 0
        losses.append(loss)
        
        if i == 14:
            data[i]['avgGain'] = avgGain = round(calculateAverage(gains[i - 14:i]), 2)
            data[i]['avgLosses'] = avgLosses = round(calculateAverage(losses[i - 14:i]), 2)
        elif i > 14:
            data[i]['avgGain'] = avgGain = round(((data[i-1]['avgGain'] * 13) + gain ) / 14, 2)
            data[i]['avgLosses'] = avgLosses = round(((data[i-1]['avgLosses'] * 13) + loss ) / 14, 2)
        if i >= 14:
            data[i]['rs'] = rs = round(avgGain / avgLosses, 2)
            data[i]['rsi'] = round(100 - (100 / (1 + rs)) , 2)
        
    return data

data = openData('../data/Oslo_STL.json')
data = calculateRSI(data)
writeData('../output/Oslo_STL_rsi.json', data)
