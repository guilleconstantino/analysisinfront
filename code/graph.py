import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from datetime import datetime

from calculate import openData, extractValues

# graph Bollinger bands during a period of 20 days
# data: must contain 'deviation', 'upperBand', 'middleBand' and 'lowerBand' values
# number: number to select data (between 39 and 439) 
def graphBollinger(data, number):
    if number > 440 or number < 39:
        raise Exception("Invalid input")
    graphData = data[number - 20:number]

    x = extractValues(graphData, 'date')
    y = extractValues(graphData, 'last', float)
    lowerBand = extractValues(graphData, 'lowerBand', float)
    upperBand = extractValues(graphData, 'upperBand', float)
    middleBand = extractValues(graphData, 'middleBand', float)
    asymmetric_error = extractValues(graphData, 'deviation', float)
    
    # Set date in correct format
    for i in range(len(x)):
        x[i] = datetime.strptime(x[i], '%Y-%m-%d')
    
    # set figure
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)
    ax.fmt_xdata = DateFormatter('%Y-%m-%d')
    fig.autofmt_xdate()
    
    # plot 4 data-set in the same graphic
    ax.plot(x, y, 'o', x, lowerBand, 'r', x, upperBand, 'r', x, middleBand, 'r--')
    # add error for y data
    plt.errorbar(x, y, yerr=asymmetric_error, fmt='o')
    
    # add some margin, to display it cleaner
    margin = 1
    x0, x1, y0, y1 = plt.axis()
    plt.axis((x0 - margin, x1 + margin, y0 - margin, y1 + margin))
    
    # show graphic
    plt.show()


# graph RSI during a period of 20 days
# data: must contain 'rsi' values
# number: number to select data (between 39 and 439) 
def graphRSI(data, number):
    if number > 440 or number < 39:
        raise Exception("Invalid input")
    graphData = data[number - 20:number]

    x = extractValues(graphData, 'date')
    y = extractValues(graphData, 'last', float)
    
    # Set date in correct format
    for i in range(len(x)):
        x[i] = datetime.strptime(x[i], '%Y-%m-%d')
    
    fig = plt.figure()
    
    # upper plot: show values
    ax = fig.add_subplot(211)
    ax.grid(True)
    ax.fmt_xdata = DateFormatter('%Y-%m-%d')
    fig.autofmt_xdate()
    ax.plot(x, y, 'r')
    
    # bottom plot: show RSI
    ax2 = fig.add_subplot(212)
    ax2.grid(True)
    ax2.fmt_xdata = DateFormatter('%Y-%m-%d')
    y2 = extractValues(graphData, 'rsi', float)
    fig.autofmt_xdate()
    OVERBOUGHT = 70
    OVERSOLD = 30
    ax2.plot(x, y2, 'b', x, [OVERSOLD] * 20, 'b--', x, [OVERBOUGHT] * 20, 'b--')
    ax2.set_ylim(bottom=0, top=100)

    # show graphic
    plt.show()
