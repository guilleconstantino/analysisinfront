import sys
from calculate import openData, writeData, extractValues, calculateBollinger, calculateRSI
from graph import graphBollinger, graphRSI

functionNames = [calculateBollinger, calculateRSI]
graphFunctionNames = [graphBollinger, graphRSI]
fileNames = ['Oslo_STL', 'Stockholm_ABB']
validOptions = ('1', '2')

print "1. Bollinger bands"
print "2. The relative strength index"
inputFunction = raw_input("Option: ")

print ""
print "1. Output data"
print "2. Graph"
inputValue = raw_input("Option: ")

print ""
print "1. Oslo"
print "2. Stockholm"
inputCity = raw_input("Option: ")

inputDate = raw_input("Select a valid date between 2014-02-25 and 2015-10-02 (yyyy-mm-dd format): ")

if inputFunction in validOptions and inputValue in validOptions and inputCity in validOptions:
    fileName = '../data/%s.json' % fileNames[int(inputCity) - 1]
    data = openData(fileName)
    
    dates = extractValues(data, 'date')
    
    # Perform Bollinger or RSI calculation
    data = functionNames[int(inputFunction) - 1](data)
    
    if inputDate in dates:
        n = dates.index(inputDate) + 1
        if int(inputValue) == 1:
            outputName = '../output/%s_%s.json' % (fileNames[int(inputCity) - 1], inputDate)
            writeData(outputName, data[n - 20:n])
        else:
            # Graph Bollinger or RSI
            graphFunctionNames[int(inputFunction) - 1](data, n)
    else:
        print "Invalid date or inexistent data for that date"
    
else:
    print "Invalid options" 
