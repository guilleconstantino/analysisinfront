INSTRUCTIONS
############

One typical feature of the software we make at Infront is the ability to perform technical analysis on financial instruments. One tool used for this purpose is 
to calculate certain functions on data presented in a chart and the plot the result in the same chart. In this test we want you to calculate and plot two such
functions, Bollinger bands and the relative strength index.

1. Bollinger bands
Bollinger bands consists of three plot-lines, a 20 day (or other period, but we use 20 day in this test) moving average as the center band, and an upper and lower band
two standard deviations above and below the center band. Bollinger bands are typically plotted together with the original data.

Details about Bollinger bands and calculation can be found here: http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:bollinger_bands

2. The relative strength index.
The RSI is an indicator that measures the speed and change of price movements. RSI is typically plotted below the original data on a separate y-axis.

Details about RSI can be found here: http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:relative_strength_index_rsi

In the data/ folder are historical prices for two stocks (Statoil on Oslo stock exchange and ABB on Stockholm stock exchange). Use one or both of these for your calculations.
Submit your solution using a platform and language of your choosing (but be prepared to defend your choice if you choose something weird :) ). Feel free to use a library to draw
your charts.


REQUIREMENTS
############
- Python (recommended version 2.7): I used Python because I have experience with this language
- matplotlib (1.4.2 >)
  > easy_install -m matplotlib
  or 
  > pip install matplotlib


HOW TO USE
############
Note: For simplicity, I assume fixed periods (for 20 days) to plot the chart. So when you select a date, 
program will plot the chart from 20 days before the selected date, until that date.

e.g.1: If you want to plot the Bollinger bands into a chart, using Oslo_STL data:
> python run.py
Select option 1 (Bollinger), then option 2 (Graph) and option 1 (Oslo). 
Finally select a date with (yyyy-mm-dd format)

e.g.2: If you want to output the RSI into a json file, using Stockholm_ABB data:
> python run.py
Select option 2 (RSI), then option 1 (output data) and option 2 (Stockholm).
Program will generate a file: '../output/Stockholm_ABB_yyyy-mm-dd.json'



Finally, answer these questions:
- There are about 500 datapoints in each file in this test. What would you do differently if there were 30000 datapoints.
The "run.py" file calculates and plot everytime we execute this file. It is a simple program, for showing
how to generate data (instead of getting the hardcoded result). 
However, to improve performance, we have to calculate results with a batch program, and just show the result.
To increase plot performance, we could put some cache, to avoid generate repeated graphs.  


- How would you accomodate real time streaming updates to the dataset.
In this case, we must considerate using another language (like Javascript). 
And we need to make dynamic calls with AJAX to refresh data in the graph.


